<?php
/**
 * Plugin Name: Driv Digital - Plugin readme
 * Plugin URI: https://drivdigital.no
 * Description: Provides an admin area with all the plugin readmes
 * Author: Driv Digital
 * Author URI: https://drivdigital.no
 * Version: 0.1.5
 * Text Domain: dd-plugin-readme
 * Domain Path: /languages
 *
 * @package dd-plugin-readme
 */

namespace Driv_Digital;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if ( ! class_exists( 'Parsedown' ) ) {
	return;
}

require_once __DIR__ . '/classes/class-plugin-readme.php';
Plugin_Readme::setup( $public_path );
