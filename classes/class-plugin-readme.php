<?php
/**
 * Plugin Readme
 *
 * @package dd-plugin-readme
 */

namespace Driv_Digital;

/**
 * Plugin Readme
 */
class Plugin_Readme {
	/**
	 * Public path
	 *
	 * @var string
	 */
	public static $public_path;

	/**
	 * Setup
	 *
	 * @param string $public_path Public path.
	 */
	public static function setup( $public_path ) {
		self::$public_path = $public_path;

		// Add system settings page.
		add_action( 'admin_menu', function () {
			add_menu_page(
				'Documentation',
				'Documentation',
				'manage_options',
				'dd-plugin-readme',
				__CLASS__ . '::admin_page',
				'dashicons-editor-help',
				0
			);
		} );

		add_action( 'admin_enqueue_scripts', __CLASS__ . '::admin_enqueue_scripts' );
	}

	/**
	 * Admin enqueue scripts
	 */
	public static function admin_enqueue_scripts() {
		$current_screen = get_current_screen();
		if ( ! $current_screen ) {
			return;
		}
		if ( 'toplevel_page_dd-plugin-readme' !== $current_screen->id ) {
			return;
		}
		wp_enqueue_style( 'dd-plugin-readme', plugins_url( dirname( __DIR__ ) . '/assets/admin.css' ) );
		wp_enqueue_script( 'dd-plugin-readme', plugins_url( dirname( __DIR__ ) . '/assets/admin.js' ) );
	}

	/**
	 * Get dirs
	 *
	 * @return array Strings with dirs that might contain a readme.txt
	 */
	public static function get_dirs() {
		$dirs   = glob( self::$public_path . '/plugins/*', GLOB_ONLYDIR );
		$dirs[] = get_template_directory();
		$dirs[] = get_stylesheet_directory();
		$wp_plugins = glob( WP_PLUGIN_DIR . '/*', GLOB_ONLYDIR );
		foreach ( $wp_plugins as $dir ) {
			$dirs[] = $dir;
		}
		return $dirs;
	}
	/**
	 * Parse meta
	 *
	 * @param  string $content Content.
	 * @return array           Assoc array with meta.
	 */
	public static function parse_meta( $content ) {
		$meta = [];
		$lines = explode( "\n", $content );
		foreach ( $lines as $line ) {
			if ( ! preg_match( '/^([^:]+):(.*)$/', $line, $matches ) ) {
				continue;
			}

			$key = strtolower( $matches[1] );
			$key = preg_replace( '/\s/', '_', $key );
			$key = preg_replace( '/[^a-z\d_]/', '', $key );

			$meta[ $key ] = trim($matches[2]);
		}
		return $meta;
	}

	/**
	 * Admin Page
	 */
	public static function admin_page() {

		echo '<div class="readme-plugin"><div class="doc-navigation"><ul></ul></div><div class="doc-sections">';
		$dirs   = self::get_dirs();
		$format = '<figure><img src="%s"/><figcaption>%s</figcaption></figure>';

		foreach ( $dirs as $dir ) {
			$file = "$dir/readme.txt";
			if ( ! file_exists( $file ) ) {
				continue;
			}

			$content = file_get_contents( $file );
			$meta    = [];
			if ( preg_match( '/===\n(.+?)\n\n/s', $content, $matches ) ) {
				$meta = self::parse_meta( $matches[1] );
			}

			if ( 'drivdigital' !== @$meta['contributors']) {
				continue;
			}
			// Remove meta data section.
			$content = preg_replace( '/===\n.+?\n\n/s', "===\n\n", $content );
			// Change WordPress markdown to regular markdown.
			$content = preg_replace( '/===(?:.* -)?(.+)===/', '#$1', $content );
			$content = preg_replace( '/==(.+)==/', '##$1', $content );
			// $content = preg_replace( '/=(.+)=/', '###$1', $content );

			// Parse screenshot section.
			if ( preg_match( '/#+ screenshots/i', $content, $matches, PREG_OFFSET_CAPTURE ) ) {
				$start = $matches[0][1] + strlen( $matches[0][0] );
				$pos   = strpos( $content, '#', $start );
				if ( $pos ) {
					$screenshots = substr( $content, $start, $pos );
				} else {
					$screenshots = substr( $content, $start );
				}
				$lines  = explode( "\n", trim( $screenshots ) );
				$images = [];
				$index  = 1;
				foreach ( $lines as $caption ) {
					$caption = preg_replace( '/^\d+\.\s*/', '', $caption );
					$caption = trim( $caption );
					$caption = htmlspecialchars( $caption );
					if ( ! $caption ) {
						continue;
					}
					$file_names = [
						$dir . "/assets/screenshot-$index.png",
						$dir . "/assets/screenshot-$index.jpg",
						$dir . "/assets/screenshot-$index.jpeg",
					];
					foreach ( $file_names as $file_name ) {
						if ( ! file_exists( $file_name ) ) {
							continue;
						}
						$images[] = sprintf( $format, plugins_url( $file_name ), $caption );
						break;
					}
					$index++;
				}

				$screenshots = "\n" . implode( "\n", $images );

				if ( $pos ) {
					$content = substr( $content, 0, $start ) . "\n[SCREENSHOTS]" . substr( $content, $pos - 1 );
				} else {
					$content = substr( $content, 0, $start ) . "\n[SCREENSHOTS]";
				}
			}

			$parsedown = new \Parsedown();
			$parsedown->setSafeMode( true );
			$html = $parsedown->text( $content );
			$html = str_replace( '[SCREENSHOTS]', "<div class=\"doc-screenshots\">$screenshots</div>", $html );
			echo '<div class="doc-section"><div class="doc-section__inner">';
			echo $html;
			echo '</div></div>';
		}
		echo '</div></div>';
	}
}
