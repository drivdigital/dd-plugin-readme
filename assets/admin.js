jQuery( function( $ ) {
	$('.doc-section h1:first-child').each( function() {
		var elem = $( this );

		// Give the tab an ID for anchor links
		var slug = elem.text()
			.toLowerCase()
			.replace( /\s/, '-' )
			.replace( /[^a-z\d\-]/g, '' )
			;

		var number = 2;
		var id = slug;
		// Generate a unique ID per panel
		while ( $( '#'+id ).length ) {
			id = slug + '-' + number;
			number++;
		}
		elem.parent().attr( 'id', id );

		var nav_elem = $('<li>').append(
			$('<a>' ).attr('href', '#' + id ).text( elem.text() )
		);
		$('.doc-navigation ul').append( nav_elem );
	} );
} );
